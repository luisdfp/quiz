## Table of contents
- [Requirements](#requirements)
- [Installation](#installation)
- [Development](#development)
    - [Compiling assets](#compiling-assets)
    - [Creating new steps](#creating-new-steps)
    - [Integrating new steps](#integrating-new-steps)
    - [Adding a new background image](#adding-a-new-background-image)
    - [Change the max value in the step indicator](#change-the-max-value-in-the-step-indicator)
    - [Specify which step directs the new step](#specify-which-step-directs-the-new-step)
- [Resources](#resources)

---
## Requirements

+ Node.js (>= v8.3.0).
+ NPM (installed with Node) or Yarn.
---
## Installation

1. Download the source code as .zip or just clone it:
```
$ git clone https://gitlab.com/luisdfp/quiz.git && cd quiz
```
2. Once downloaded, you need to install the dependencies:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;using NPM:
```
$ npm install
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;using Yarn:
```
$ yarn install
```
---
## Development

Once the dependencies are installed, you are ready to start developing.

There are two folders: 

+ ```public/``` - This is where the template and the main files of the quiz are found. These files are ```js/quiz.js``` and ```css/quiz.css```. Both of these files are compiled. You don't have to mess with them.
+ ```resources/``` - This is where the development files are. These files will be compiled and generate the aforementioned files found in the ```public/``` folder.

In order to compile the files, we will use Laravel-mix, which is a wrapper for Webpack (don't worry about this detail though). 

Every time we change a file in the ```resorces/``` folder, we need to recompile to generate the quiz files in the ```public/``` folder.

### Compiling assets

Luckily, we can use Webpack to watch for every change that occurs in our development files. This way, the files will be recompiled every time we change a file and save it.

To start watching for file changes, just run:

```
$ npm run watch
```

You should see something like this:

```
 DONE  Compiled successfully in 7590ms                                                                                                                                 21:23:02

                     Asset       Size  Chunks                    Chunk Names
 /public/assets/js/quiz.js     476 kB       0  [emitted]  [big]  /public/assets/js/quiz
public/assets/css/quiz.css  149 bytes       0  [emitted]         /public/assets/js/quiz
```
... and every time you save changes on a file.

That is all we need to start developing!

### Creating new steps

Let's create a simple quiz step. It will only contain a number input to introduce
your age.

1. Go to ```resources/components/steps/```
2. Add a new file, let's call it: ```AgeStep.vue```
1. Let's use ```ExampleStep.vue``` as boilerplate. Copy its content and paste it inside our new file.
4. Let's edit the file to our needs. 

&nbsp;&nbsp;&nbsp;Before:
```html
<template>
  <!-- Change the "title" and "current-page" attributes -->
  <quiz-step title="STEP TITLE GOES HERE" :current-page="1"> 
      <div slot="content">
          <!-- Here goes the content of your step (inputs, text boxes, options)-->   
      </div>
      <div slot="actions">
          <!-- Here goes the "actions", like the "Next" button -->
          <next-button to="/ROUTE-TO-NEXT-STEP"></next-button>    
      </div>         	
  </quiz-step>
</template>

<script>
import QuizStep from "../QuizStep.vue"
import NextButton from "../NextButton.vue"

export default {
  components: {
    "quiz-step": QuizStep,
    "next-button": NextButton
  }
};
</script>

<style>
/* Add css for this step to your liking */
</style>
```
&nbsp;&nbsp;&nbsp;After:
```html
<template>
    <quiz-step title="What is your age?" :current-page="5">
        <div slot="content">
            <div class="age-question">
                <!-- More info about how the v-model directive works at:
                    https://vuejs.org/v2/guide/components.html#Customizing-Component-v-model  
                 -->
                <input type="number" name="age" min="0" max="120" v-model="age">
                <div>
                    You are {{ age }} years old.
                </div>
            </div>
        </div>
        <div slot="actions">
            <!-- The "to" attribute will be the route to the next step, I will explain this ahead -->
            <next-button to="/quiz-step-price"></next-button>
        </div>
    </quiz-step>
</template>

<script>
import QuizStep from "../QuizStep.vue"
import NextButton from "../NextButton.vue"

export default {
  components: {
    "quiz-step": QuizStep,
    "next-button": NextButton
  },
  data() {
    return {
      age: 18
    };
  }
};
</script>

<style>
.age-question {
  text-align: center;
}
.age-question input {
    margin-bottom: 10px
}
</style>
```

5. Export this new step in ```resources/js/components/steps/index.js```:

```js
// resources/js/components/steps/index.js
import PriceStep from './PriceStep.vue'
import LocationStep from './LocationStep.vue'
import LeaseLengthStep from './LeaseLengthStep.vue'
import UrgencyStep from './UrgencyStep.vue'
import AgeStep from './AgeStep.vue' // <--- here


export { PriceStep, LocationStep, LeaseLengthStep, UrgencyStep, AgeStep } //<- here
```

Our new step is finished but, we still haven't specified where in the quiz it will go or be accessed.

### Integrating new steps

To make our `AgeStep.vue` accessible, first we need to register a route for it.

1. Go to `resources/js/routes.js`
2. Add a route for the new step, we will call it `/quiz-step-age`:

```js
// routes.js
import { 
  PriceStep, 
  LocationStep, 
  LeaseLengthStep,
  UrgencyStep,
  AgeStep // <- we import the component
} from './components/steps'

export const routes = [
  { path: '/quiz-step-price', component: PriceStep },
  { path: '/quiz-step-location', component: LocationStep },
  { path: '/quiz-step-lease', component: LeaseLengthStep },
  { path: '/quiz-step-urgency', component: UrgencyStep },
  { path: '/quiz-step-age', component: AgeStep }, // <- and we map it to a route
  { path: '/', redirect: '/quiz-step-price' }
]
```

Now we can access our component by the page URL. Since I am developing locally, I can just go to e.g `http://127.0.0.1:8887/search-job.html#/quiz-step-age` and it will show me the new step.

You may notice though:

+ There is no background image.
+ It says `5/4` in the step indicator, which is wrong.
+ We still haven't specified which step directs to this step

 ### Adding a new background image

1. Go to `Quiz.vue`.
2. Add a new background image:

```html
<!-- resources/js/components/Quiz.vue -->
    <background-image 
        key="4"
        v-if="currentRoute === '/quiz-step-urgency'" 
        src="https://res.cloudinary.com/apartmentlist/image/upload/s---w4WcHcU--/t_web-base/v1/web/static/quiz/maxrent"
    ></background-image>

    <!--** Added ** -->
    <!-- Notice the age step route in the v-if directive -->
    <!-- The "key" attribute must be unique in relation to the other background-image elements -->
    <background-image 
        key="5" v-if="currentRoute === '/quiz-step-age'" 
        src="background-image-src" 
    ></background-image>
    <!-- ** Added ** -->
</transition>
```

### Change the max value in the step indicator

In `QuizStep.vue`, simply change the default value of the `maxPage` prop:

```js
maxPage: {
    type: Number,
    default: 5 // from 4 to 5
}
```
Yes, this is hardcoded. But it's no big deal for now. :)

### Connecting steps

Simply choose which step you want to precede the new step and adjust the ```next-button``` of that step:

```html
<!-- In another step, e.g: PriceStep.vue -->
<next-button to="/quiz-step-age"></next-button>
```

---

## Resources
More information about how vue.js works: https://vuejs.org/v2/guide/