import { 
  PriceStep, 
  LocationStep, 
  LeaseLengthStep,
  UrgencyStep,
  AgeStep
} from './components/steps'

export const routes = [
  { path: '/quiz-step-price', component: PriceStep },
  { path: '/quiz-step-location', component: LocationStep },
  { path: '/quiz-step-lease', component: LeaseLengthStep },
  { path: '/quiz-step-urgency', component: UrgencyStep },
  { path: '/quiz-step-age', component: AgeStep },
  { path: '/', redirect: '/quiz-step-price' }
]