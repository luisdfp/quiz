import Vue from 'vue'
import VueRouter from 'vue-router'

import {routes} from './routes'
import {store} from './store'
import Quiz from './components/Quiz.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

new Vue({
  el: '#quiz',
  router,
  store,
  render: (h) => h(Quiz)
})
