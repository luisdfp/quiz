const mix = require('laravel-mix');

const JS_SRC = 'resources/js'
const JS_TRG = 'public/assets/js'

const CSS_SRC = 'resources/sass'
const CSS_TRG = 'public/assets/css'

mix.js(`${JS_SRC}/quiz.js`, JS_TRG)
   .sass(`${CSS_SRC}/quiz.scss`, CSS_TRG);